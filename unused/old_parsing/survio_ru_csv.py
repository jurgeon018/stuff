import re
import csv
import requests
from bs4 import BeautifulSoup


def get_html(url):
    r = requests.get(url)
    return r.text if r.ok else print(r.status_code)



def write_csv(data):
    with open('survio/test.csv', 'a') as f:
        order = ['title', 'ua_label', 'ru_label', 'en_label', 'ua_values', 'ru_values', 'en_values', 'type', 'category', 'required', 'ua_placeholder', 'ru_placeholder', 'en_placeholder']
        writer = csv.DictWriter(f, fieldnames=order)
        writer.writerow(data)


def find_header(fieldset): # заголовок вопроса
    header = fieldset.find('div', class_='title-part').text.split('\n')[1].strip()
    if header.isdigit():
        header = fieldset.find('div', class_='title-part').text.split('\n')[2].strip()
        return header
    else:
        header = fieldset.find('div', class_='title-part').text.split('\n')[1].strip()
        return header


def parse_one_page(url, category=None):
    soup = BeautifulSoup(get_html(url), 'lxml')
    fieldsets = soup.find_all('fieldset')
    for fieldset in fieldsets:

        title = soup.find('header', class_='title').\
                     find('div', class_='col-title').\
                     h1.text # заголовок анкеты
        ru_label = find_header(fieldset) # заголовок вопроса

        required = 'TRUE' if fieldset.h2.find('div', class_='title-part').find('div') else 'FALSE'
        # radio-button со стикерами
        try:
            ru_values = []
            type = 'radio-button'
            category = category
            divs = fieldset.find_all('div', class_='col-xs-12 col-sm-6 col-md-4 col-pad-md-left-0 col-pad-sm-left-0 col-xs-pad-0 input-image-group')
            for div in divs:
                option = div.find('label', class_='input-group input-group-radio').find('span', class_='input-group-title').text
                ru_values.append('{"label":"'+option+'", "value":"'+option+'"}')
                # print(ru_values)
            data  = {'title':title,
                           'ua_label':'',
                           'ru_label':ru_label,
                           'en_label':'',
                           'ua_values':'',
                           'ru_values':''+ru_values,
                           'en_values':'',
                           'type':type,
                           'category':category,
                           'required':required,
                           'ua_placeholder':'',
                           'ru_placeholder':'',
                           'en_placeholder':''}
            write_csv(data)
        except Exception as e1:
            # print('e1', e1)
            pass

        # # star
        try:
            ru_values = []
            type = 'radio-button'
            category = category
            stars = fieldset.find('div', class_='row').\
            find('div', class_='col-xs-12 col-sm-12 col-md-12 col-pad-0 col-radius-3 original-stars').find_all('input', class_='star')
            for star in stars:
                option = star.get('value')
                ru_values.append('{"label":"'+option+'", "value":"'+option+'"}')
            data   = {'title':title,
                           'ua_label':'',
                           'ru_label':ru_label,
                           'en_label':'',
                           'ua_values':'',
                           'ru_values':''+','.join(ru_values),
                           'en_values':'',
                           'type':type,
                           'category':category,
                           'required':required,
                           'ua_placeholder':'',
                           'ru_placeholder':'',
                           'en_placeholder':''}
            write_csv(data)
        except Exception as e2:
            # print('e2',e2)
            pass

        # placeholder textarea
        try:
            ru_values = ''
            type = 'textarea'
            category = category
            ru_placeholder = fieldset.\
                                    find('label', class_='input-group').\
                                    find('textarea', class_='form-control').\
                                    get('placeholder')
            data   = {'title':title,
                           'ua_label':'',
                           'ru_label':ru_label,
                           'en_label':'',
                           'ua_values':'',
                           'ru_values':''+ru_values,
                           'en_values':'',
                           'type':type,
                           'category':category,
                           'required':required,
                           'ua_placeholder':'',
                           'ru_placeholder':ru_placeholder,
                           'en_placeholder':''}
            write_csv(data)
        except Exception as e3:
            # print('e3', e3)
            pass
        # placeholder input
        try:
            ru_values = ''
            type = 'text'
            category = category
            ru_placeholder = fieldset.\
                                    find('label', class_='input-group').\
                                    find('input', class_='form-control').\
                                    get('placeholder')
            data  = {'title':title,
                           'ua_label':'',
                           'ru_label':ru_label,
                           'en_label':'',
                           'ua_values':'',
                           'ru_values':''+ru_values,
                           'en_values':'',
                           'type':type,
                           'category':category,
                           'required':required,
                           'ua_placeholder':'',
                           'ru_placeholder':ru_placeholder,
                           'en_placeholder':''}
            write_csv(data)
        except Exception as e4:
            # print('e4',e4)
            pass

        # select
        try:
            ru_values = []
            type = 'select'
            category = category
            options = fieldset.find('label', class_='select').find('select', class_='form-control').find_all('option')#[1:]
            for option in options:
                answers = option.text
                ru_values.append('{"label":"'+answers+'", "value":"'+answers+'"}')
            data  = {'title':title,
                           'ua_label':'',
                           'ru_label':ru_label,
                           'en_label':'',
                           'ua_values':'',
                           'ru_values':''+','.join(ru_values),
                           'en_values':'',
                           'type':type,
                           'category':category,
                           'required':required,
                           'ua_placeholder':'',
                           'ru_placeholder':'',
                           'en_placeholder':''}
            write_csv(data)
        except Exception as e5:
            # print('e5', e5)
            pass

        # radio-button - ДУБЛИРУЕТ???
        try:
            ru_values = []
            type = 'radio-button'
            category = category
            labels = fieldset.find('div', class_='label-cont').find_all('label', class_='input-group input-group-radio row')

            for label in labels:
                answers = label.find('span', class_='input-group-title').text
                ru_values.append('{"label":"'+answers+'", "value":"'+answers+'"}')
            data = {'title':title,
                           'ua_label':'',
                           'ru_label':ru_label,
                           'en_label':'',
                           'ua_values':'',
                           'ru_values':''+','.join(ru_values),
                           'en_values':'',
                           'type':type,
                           'category':category,
                           'required':required,
                           'ua_placeholder':'',
                           'ru_placeholder':'',
                           'en_placeholder':''}
            write_csv(data)
        except Exception as e6:
            # print('e6',e6)
            pass

        # # checkbox
        # try:
        #     ru_values = []
        #     type = 'checkbox'
        #     category = category
        #     labels = fieldset.find('div', class_='label-cont').find_all('label', class_='input-group input-group-checkbox row')
        #     for label in labels:
        #         answers = label.find('span', class_='input-group-title').text
        #         ru_values.append('{"label":"'+answers+'", "value":"'+answers+'"}')
        #     data = {'title':title,
        #                    'ua_label':'',
        #                    'ru_label':ru_label,
        #                    'en_label':'',
        #                    'ua_values':'',
        #                    'ru_values':','.join(ru_values),
        #                    'en_values':'',
        #                    'type':type,
        #                    'category':category,
        #                    'required':required,
        #                    'ua_placeholder':'',
        #                    'ru_placeholder':'',
        #                    'en_placeholder':''}
        #     write_csv(data)
        # except Exception as e6:
        #     # print('e6',e6)
        #     pass

        # # Сложная таблица
        # try:
        #     ru_values = ''
        #     type = ''
        #     category = category
        #     required = ''
        #     divs = fieldset.find('div', class_='matrix-values').\
        #         find_all('div', class_='input-group input-group-matrix row')
        #     for div in divs:
        #         answer = div.find('div', class_='col-sm-4').find('div', class_='title').text
        #         print(answer)
        #         data = {'title':title,
        #                        'ua_label':'',
        #                        'ru_label':ru_label,
        #                        'en_label':'',
        #                        'ua_values':'',
        #                        'ru_values':ru_values,
        #                        'en_values':'',
        #                        'type':type,
        #                        'category':category,
        #                        'required':required,
        #                        'ua_placeholder':'',
        #                        'ru_placeholder':'',
        #                        'en_placeholder':''}
        #         write_csv(data)
        # except Exception as e7:
        #     # print('e7',e7)
        #     pass


def parse_many_pages(url):
    soup = BeautifulSoup(get_html(url), 'lxml')

    research = soup.find('div', class_='category research').find('ul').find_all('li')
    feedback = soup.find('div', class_='category feedback').find('ul').find_all('li')
    events = soup.find('div', class_='category events').find('ul').find_all('li')
    community = soup.find('div', class_='category community').find('ul').find_all('li')
    other = soup.find('div', class_='category other').find('ul').find_all('li')
    services = soup.find('div', class_='category services').find('ul').find_all('li')
    hr = soup.find('div', class_='category hr').find('ul').find_all('li')
    education = soup.find('div', class_='category education').find('ul').find_all('li')
    health = soup.find('div', class_='category health').find('ul').find_all('li')

    # for li in research:
    # for li in feedback:
    # for li in events:
    # for li in community:
    # for li in other:
    # for li in services:
    # for li in hr:
    # for li in education:
    for li in health:
        try:
            ru_label = li.a.text.replace('/', ',')
            category = ([i for i in list(li.a.parent.parent.parent)[2]][0])
            url = li.a.get('href')
            parse_one_page(url, category)
        except:
            pass


def execute_parsing():
    # with open('survio/test.csv', 'a') as f:
    #     writer = csv.writer(f)
    #     writer.writerow(['title', 'ua_label', 'ru_label', 'en_label', 'ua_values', 'ru_values', 'en_values', 'type', 'category', 'required', 'ua_placeholder', 'ru_placeholder', 'en_placeholder'])

    # parse_one_page('https://www.survio.com/shablon-oprosa/otsenka-magazina-optiki-optik-studii')
 
    parse_many_pages('https://www.survio.com/ru/shablony-oprosov')
    # parse_many_pages('https://www.survio.com/en/survey-templates')


def main():
    execute_parsing()

if __name__ == '__main__':
    main()
