import re

'''
<h2><img alt="" src="https://c.radikal.ru/c14/1712/02/2240c7098bf1.png" style="width:250px;height:159px" /> <img alt="" src="https://my.prom.ua/media/images/1036085090_w640_h2048_dostavka_2.png?PIMAGE_ID=1036085090" style="width:250px;height:160px" /> <a href="https://sport-sneakers.com.ua/product_list"><img alt="" src="https://d.radikal.ru/d24/1712/39/3085dce06b5c.png" style="width:250px;height:159px" /></a></h2> <h2 style="text-align:center">Adidas NMD_R1 Red & White</h2> <h3 style="text-align:center"> &bull; Нужный размер укажите в комментарии к заказу.<br /> &bull; Доставка заказа от 5 до 8дней.<br /> &bull; Оригинальная продукция 100%.</h3> <p style="text-align:center"><span style="color:rgb(63, 76, 82);font-size:14px"></span></p><p style="text-align:center"></p>
'''

with open('./tags.csv') as file:
  s = file.read()
  
# s = '<h2><img alt="" src="https://c.radikal.ru/c14/1712/02/2240c7098bf1.png" style="width:250px;height:159px" /> <img alt="" src="https://my.prom.ua/media/images/1036085090_w640_h2048_dostavka_2.png?PIMAGE_ID=1036085090" style="width:250px;height:160px" /> <a href="https://sport-sneakers.com.ua/product_list"><img alt="" src="https://d.radikal.ru/d24/1712/39/3085dce06b5c.png" style="width:250px;height:159px" /></a></h2> <h2 style="text-align:center">Adidas NMD_R1 Red & White</h2> <h3 style="text-align:center"> &bull; Нужный размер укажите в комментарии к заказу.<br /> &bull; Доставка заказа от 5 до 8дней.<br /> &bull; Оригинальная продукция 100%.</h3> <p style="text-align:center"><span style="color:rgb(63, 76, 82);font-size:14px"></span></p><p style="text-align:center"></p>'

start1 = [i for i in range(len(s)) if s[i:i+4] == '<img']
end1 = [i for i in range(len(s)) if s[i:i+2] == '/>']

def find_all(a_str, sub):
  start = 0
  while True:
      start = a_str.find(sub, start)
      if start == -1: break 
      yield start
      start += len(sub) # use start += 1 to find overlapping matches

start2 = list(find_all(s, '<img'))
end2 = list(find_all(s, '/>'))

print(start1)
print(end1)
# print(start2)
# print(end2)


# for i in range(len(start1)):
#   print(s[:start1[i]] + s[end1[i]:])


# s = '<img sdf /> @ """@$ FSDF something something <img aaa ssss/>'
new = re.sub('![^>]+!', '', s)
new = re.sub(r'<.+?>', '', s)
new = re.sub(r'\<img[^>]*\ />', '', s)
print(new)

with open('new_tags.csv', 'w') as file:
  file.write(new.strip(' '))
