import requests
from bs4 import BeautifulSoup
from random import choice


def get_proxy():
    html = requests.get('https://free-proxy-list.net/').text
    soup = BeautifulSoup(html, 'lxml')
    trs = soup.find('table', id='proxylisttable').find_all('tr')[100:150]
    proxies = []
    for tr in trs:
        tds = tr.find_all('td')
        ip = tds[0].text.strip()
        port = tds[1].text.strip()
        schema = 'https' if 'yes' in tds[6].text.strip() else 'http'
        proxy = {'schema': schema, 'address': ip + ':' + port}
        proxies.append(proxy)
    return choice(proxies)

def get_html(url):
    p = get_proxy()
    proxy = { p['schema']: p['address']  }
    r = requests.get(url, proxies=proxy)
    return r.text if r.ok else print(r.status_code)

def parse_1_page(url, filename=None, foldername=None):
    soup = BeautifulSoup(get_html(url), 'lxml')

    filename = soup.find('h1', class_='header-survey__title').text
    header = filename + '\n\t{}'.format(url)

    div_questions = soup.find('form', id='fm-questions').find_all('div', class_='question-container')

    write_file(('\t\t\tТема шаблона:'+header), filename, foldername)

    for question in div_questions:
            question_header = "\nвопрос:{}".format(question.div.h3.text)

            write_file(question_header, filename, foldername)


            # input
            try:
                answer = question.find('div', class_='questionFormArea').find_all('input', type='text', class_='decor-radio__type1-input')[0]
                answer = 'введите текст'
                write_file('\t'+answer, filename, foldername)
            except:
                pass

            # TextArea
            try:
                answers = question.find('div', class_='questionFormArea').find_all('textarea', class_='decor-radio__type1-textarea')[0]
                answer = 'заполните поле'
                write_file('\t'+answer, filename, foldername)
            except:
                pass

            # radio
            try:
                answers = question.find('div', class_='questionFormArea').find('ul', class_='answer-ul').find_all('li')
                for li in answers:
                    answer = li.find('span', class_='decor-radio__type1-text').text
                    write_file('\to:'+answer, filename, foldername)
            except:
                pass


            # checkbox
            try:
                answers = question.find('div', class_='questionFormArea').find('ul', class_='answer-ul').find_all('li')
                for li in answers:
                    # answer = li.find('span', class_='decor-checkbox__type3-text').text
                    answer = li.find_all('span', class_='decor-checkbox__type3-text')[-1].text
                    write_file('\t_:'+answer, filename, foldername)
            except:
                pass


            # table
            try:
                answers = question.find('div', class_='questionFormArea')

                tds = answers.find('div', class_='section-two-table').find('table', class_='answer-table skip-logic').find_all('tr')[0].find_all('td')
                upper_row = [td.div.text for td in tds]
                answer = '  |  '.join(upper_row)
                write_file('\t\t'+answer, filename, foldername)

                trs = answers.find('table', class_='section-one-table').find_all('tr')[1:]
                left_col = [tr.find('td').text+':' for tr in trs]
                answer = '\n'.join(left_col)
                write_file(answer, filename, foldername)
            except Exception as e1:
                pass


            # 1 to 5
            try:
                tds = question.find('div', class_='questionFormArea').find('div', class_='section-scale-table').find('table').find('tr').find_all('td')
                answers = [td.find('div').text.strip() for td in tds]
                write_file('  '+'\t'.join(answers), filename, foldername)
                hint = question.find('div', class_='commentary ajustify').p.em.text
                write_file(f'({hint})', filename, foldername)

            except:
                pass

            # select
            try:
                options = question.find('div', class_='questionFormArea').find('select').find_all('option')[1:]
                for option in options:
                    answer = option.text
                    write_file('\t'+answer, filename, foldername)
            except:
                pass

            try:
                tds = question.find('div', class_='questionFormArea').find('table').find('tr').find_all('td')
                for td in tds:
                    answer = td.find('span').text.strip()
                    write_file('\t'+answer, filename, foldername)
            except:
                pass

            # try:
            #     answers = question.find('')
            #     for i in answers:
            #         anser = i.find('')
            #         write_file('\t'+answer, filename, foldername)
            # except:
            #     pass


            # try:
            #     answers = question.find('')
            #     for i in answers:
            #         anser = i.find('')
            #         write_file('\t'+answer, filename, foldername)
            # except:
            #     pass


            # try:
            #     answers = question.find('')
            #     for i in answers:
            #         anser = i.find('')
            #         write_file('\t'+answer, filename, foldername)
            # except:
            #     pass


def parse_many_pages():
    soup = BeautifulSoup(get_html('https://www.testograf.ru/ru/provedenie/primeri-oprosov/'), 'lxml')
    clients = soup.find_all('div', class_='templates-left templates-left_for-clients')
    marketing = soup.find_all('div', class_='templates-right templates-right_for-marketing')
    education = soup.find_all('div', class_='templates-right templates-right_for-education')
    staff = soup.find_all('div', class_='templates-left templates-left_for-staff')
    nocommerce = soup.find_all('div', class_='templates-right templates-right_for-no-commerce')
    health = soup.find_all('div', class_='templates-left templates-left_for-live')
    business = soup.find_all('div', class_='templates-right templates-left_for-business')

    all = [clients, marketing, education, staff, nocommerce, health, business]

    for i in nocommerce:
        foldername = i.find('h3').strong.text
        lis = i.find('ul').find_all('li')
        for li in lis:
            link = 'https://www.testograf.ru'+li.p.a.get('href')
            filename = li.p.a.text
            parse_1_page(link, filename, foldername)
            # print(filename)

def write_file(data, filename, foldername):
    # foldername='Примеры анкет для сотрудников'
    # foldername += '/Трудоустройство'
    # foldername += '/Оценка, самооценка'
    # foldername += '/Удовлетворенность'
    # foldername += '/Лояльность, вовлеченность, мотивация'
    # foldername += '/Коллектив'
    # foldername += '/Прочее'
    # foldername += '/Уходящий сотрудник'
    print(data)
    with open(f'testograf/{foldername}/{filename}.txt', 'a') as f:
        print(data, file=f)

def execute():


    # url = 'https://www.testograf.ru/ru/template/aktualnie/priema-na-rabotu.html'
    # url = 'https://www.testograf.ru/ru/template/aktualnie/opros-dlya-soiskatelya-dolzhnosti.html'
    # url = 'https://www.testograf.ru/ru/template/aktualnie/sbor-rezume.html'

    # url = 'https://www.testograf.ru/ru/template/aktualnie/anketa-kandidata4.html'
    # url = 'https://www.testograf.ru/ru/template/aktualnie/anketa-kandidata1.html'
    url = 'https://www.testograf.ru/ru/template/aktualnie/anketa-kandidata.html'


    parse_1_page(url)
    # parse_many_pages()

def main():
    execute()


if __name__ == '__main__':
    main()
