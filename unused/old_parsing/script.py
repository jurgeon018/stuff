import requests
from bs4 import BeautifulSoup
import csv

def get_html(url):
    r = requests.get(url)
    return r.text if r.ok else print(r.status_code)

def get_data(html):
    soup = BeautifulSoup(html, 'lxml')
    ru_values = []
    fieldsets = soup.find_all('fieldset')
    for fieldset in fieldsets:
        required = 'TRUE' if fieldset.h2.find('div', class_='title-part').find('div') else 'FALSE'
        print(required)




def write_csv(data):
    with open('test.csv', 'a') as f:
        order = ['title']
        writer = csv.DictWriter(f, fieldnames=order)
        writer.writerow(data)


def main():
    get_data(get_html('https://www.survio.com/shablon-oprosa/khod-domashnego-khozyaystva'))

if __name__ == '__main__':
    main()
