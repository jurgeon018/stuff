# sudo apt-get install postgresql && pgadmin3
# sudo -u postgres psql
# CREATE DATABASE test;
# \l
# sudo pip3 install peewee psycopg2 psycopg2-binary
import requests
from bs4 import BeautifulSoup
import csv
from peewee import *


def get_html(url):
    r = requests.get(url)
    return r.text


def write_csv(data):
    with open('wordpress.csv', 'a') as f:
        #   csv.DictWriter
        order = ['header','link','rating','desc','block_header']
        writer = csv.DictWriter(f, fieldnames=order)
        writer.writerow(data)
        #   csv.writer
        # writer = csv.writer(f)
        # writer.writerow([data['name'],
        #                  data['symbol'],
        #                  data['url'],
        #                  data['price']])


def write_db():
    db = SqliteDatabase('wordpress.db')
    # db = PostgresqlDatabase(database='test1', user='postgres', password='69018', host='localhost')
    # db = MySQLDatabase('my_app', user='app', password='db_password',  host='10.1.0.8', port=3316)

    class Plugins(Model):
        class Meta:
            database = db
        header = CharField()
        link = TextField()
        rating = IntegerField()
        desc = TextField()
        block_header = CharField()

    db.connect()
    db.create_tables([Plugins])

    with open('wordpress.csv', 'r') as f:
        order = ['header','link','rating','desc','block_header']
        reader = csv.DictReader(f, fieldnames=order)
        plugins = list(reader)

        with db.atomic():
            for plugin in plugins:
                Plugins.create(**plugin)

        # with db.atomic():
        #     for i in range(0, len(plugins), 100):
        #         Plugins.insert_many(plugins[i:i+100]).execute()

        # for row in coins:
        #     coin = Coin(name=row['name'], url=row['url'], price=row['price'])
        #     coin.save()



def get_data(html):
    soup = BeautifulSoup(html, 'lxml')
    block_header = soup.find('h1', class_='page-title').text
    try:
        block_description = soup.find('div', class_='taxonomy-description').p.text
    except:
        block_description = ''
    articles = soup.find_all('article', class_='plugin-card')
    for article in articles:
        header = article.find('h2', class_='entry-title').a.text
        link =  article.find('h2', class_='entry-title').a.get('href')
        desc = article.find('div', class_='entry-excerpt').p.text
        rating = article.find('span', class_='rating-count').a.text.split()[0].strip().replace(',','')
        data = {'header':header, 'link':link, 'rating':rating, 'desc':desc, 'block_header':block_header}
        write_csv(data)


def execute_parsing():
    soup = BeautifulSoup(get_html("https://wordpress.org/plugins"), 'lxml')
    links = [i.get('href') for i in soup.find_all('a', class_='section-link')]
    print(links)
    for url in links:
        while True:
            get_data(get_html(url))
            soup = BeautifulSoup(get_html(url), 'lxml')
            try:
                url = soup.find('a', class_='next page-numbers').get('href')
            except Exception as e:
                print(e)
                break


def main():
    # execute_parsing()
    # write_db()


if __name__ == '__main__':
    main()
