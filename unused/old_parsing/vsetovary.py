import re
import csv
import requests
from bs4 import BeautifulSoup
from random import choice


def get_proxy():
    html = requests.get('https://free-proxy-list.net/').text
    soup = BeautifulSoup(html, 'lxml')
    trs = soup.find('table', id='proxylisttable').find_all('tr')[50:100]
    proxies = []
    for tr in trs:
        tds = tr.find_all('td')
        ip = tds[0].text.strip()
        port = tds[1].text.strip()
        schema = 'https' if 'yes' in tds[6].text.strip() else 'http'
        proxy = {'schema': schema, 'address': ip + ':' + port}
        proxies.append(proxy)
    return choice(proxies)


def get_html(url):
    p = get_proxy()
    proxy = { p['schema']: p['address']  }
    print(proxy)
    r = requests.get(url, proxies=proxy)
    return r.text if r.ok else print(r.status_code)


def write_csv(data):
    with open('vsetovary.csv', 'r') as f:
        file = f.read()
        if data['name'] not in file:
            with open('vsetovary.csv', 'a') as f:
                order = ['name', 'price', 'is_available', 'way', 'cat_name', 'subcat_name']
                writer = csv.DictWriter(f, fieldnames=order)
                writer.writerow(data)

def get_categories():
    url = 'https://vsetovary.com.ua/product_list'
    soup = BeautifulSoup(get_html(url), 'lxml')
    print(soup)
    cats = soup.find('div', class_='b-layout__product-groups').ul.find_all('li')
    cat_links = {}
    for cat in cats:
        div = cat.find('div', class_='b-product_groups__info')
        if div != None:
            cat_name = div.a.text
            cat_link = 'https://vsetovary.com.ua'+div.a.get('href')
            cat_links.update({cat_link:cat_name})
    for cat_link, cat_name in cat_links.items():
        soup = BeautifulSoup(get_html(cat_link), 'lxml')
        try:subcats = soup.find('ul', class_='b-product-groups b-product-groups_view_gallery').find_all('li')
        except: pass
        for subcat in subcats:
            div = subcat.find('div', class_='b-product_groups__info')
            if div != None:
                if cat_name == 'Портативная акустика, мини колонки':
                    subcat_name = 'Портативная акустика, мини колонки'
                    subcat_link = 'https://vsetovary.com.ua/g18607381-portativnaya-akustika-mini'
                else:
                    subcat_name = div.a.text
                    subcat_link = 'https://vsetovary.com.ua' + div.a.get('href')
                # print(subcat_link)
                parse_many_pages(subcat_link, cat_name,
                                                 cat_link,
                                                 subcat_name,
                                                 subcat_link)

def parse_many_pages(url, *args, **kwargs):
    try:
        while True:
            print(url)
            # print(args)
            parse_one_page(get_html(url), *args, **kwargs)
            soup = BeautifulSoup(get_html(url), 'lxml')
            pagination = soup.find('div', class_="b-catalog-panel__pagination").find('a', class_="b-pager__link b-pager__link_pos_last").get('href')
            url = 'https://vsetovary.com.ua'+pagination
    except Exception as e: print(e)


def parse_one_page(html, *args, **kwargs):
    soup = BeautifulSoup(html, 'lxml')
    products = soup.find('div', class_='b-layout__clear').find_all('div', class_='b-product-line__details-panel')
    cat_name = args[0]
    cat_link = args[1]
    subcat_name = args[2]
    subcat_link = args[3]
    for product in products:
        name = product.find('div', class_='b-product-line__product-name').a.text
        try:price = product.find('div', class_='b-product-line__price-bar').find('div', class_='b-product-line__price').text.strip()
        except:price = 'Цену уточняйте'
        is_available = product.find('div', class_='b-product-line__data').find('span', class_='b-product-line__state').text
        try:way = product.find('div', class_='b-product-line__data').find('span', class_='b-product-line__selling-type').text
        except:way = ''
        # print(way)
        print(f'{name}|{price}|{is_available}|{way}|{cat_name}|{subcat_name}')
        data = {
            'name': name,
            'price': price,
            'is_available': is_available,
            'way': way,
            'cat_name': cat_name,
            'subcat_name': subcat_name,
        }
        write_csv(data)

def execute_parsing():
    url = 'https://vsetovary.com.ua/g15640843-sanki'
    # url = 'https://vsetovary.com.ua/g18607381-portativnaya-akustika-mini'
    # parse_one_page(get_html(url))
    # parse_many_pages(url)
    # get_categories()
    soup = BeautifulSoup(get_html(url), 'lxml')
    print(soup)

def main():
    execute_parsing()

if __name__ == '__main__':
    main()
