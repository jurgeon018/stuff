import re
import csv
import requests
from bs4 import BeautifulSoup


def get_html(url):
    r = requests.get(url)
    return r.text if r.ok else print(r.status_code)

def write_file(data, filename, foldername):
    with open('survio/{}/{}.txt'.format(foldername, filename), 'a') as f:
        print(data, file=f)
        # with open('survio/{1}.txt'.format(foldername, filename), 'a') as f:
        #     print(data, file=f)

def parse_one_page(url, filename=None, foldername=None):
    soup = BeautifulSoup(get_html(url), 'lxml')
    main_title = soup.find('header', class_='title').find('div', class_='col-title').h1.text + '\n\t({})'.format(url)
    filename = soup.find('header', class_='title').find('div', class_='col-title').h1.text

    write_file(('\t\t\tТема шаблона:'+main_title), filename, foldername)
    fieldsets = soup.find_all('fieldset')
    for fieldset in fieldsets:

        # header = "\nвопрос:{}".format(fieldset.find('h2').find('div', class_='title-part').text.split('\n')[1].strip())
        header = fieldset.find('div', class_='title-part').text.split('\n')[1].strip()
        if header.isdigit():
            header = h.find('div', class_='title-part').text.split('\n')[2].strip()
            print(header,':',header.isdigit())
            data = {'title':header}
            write_csv(data)
        else:
            header = fieldset.find('div', class_='title-part').text.split('\n')[1].strip()
            print(header,':',header.isdigit())
            data = {'title':header}
            write_csv(data)

        try:
            divs = fieldset.find_all('div', class_='col-xs-12')
            # print('***',header)
            # write_file(header, filename, foldername)
            for div in divs:
                answer = div.find('label', class_='input-group input-group-radio').find('span', class_='input-group-title').text
                print(answer)
                write_file('\t'+answer, filename, foldername)
        except Exception as e5:
            pass

        try:
            stars = fieldset.find('div', class_='row').find('div', class_='col-xs-12 col-sm-12 col-md-12 col-pad-0 col-radius-3 original-stars').find_all('input', class_='star')
            for star in stars:
                # result.append(star.get('value'))
                result = star.get('value')
            print(header)
            write_file(header, filename, foldername)
            write_file('\tВыберите количество звездочек от 1 до 5-ти', filename, foldername)
        except Exception as e4:
            pass


        try:
            placeholder_text =  fieldset.find('label', class_='input-group').find('input', class_='form-control').get('placeholder')
            print(header)
            print(placeholder_text)
            write_file(header, filename, foldername)
            write_file('\t'+placeholder_text, filename, foldername)
        except Exception as e3:
            pass

        try:
            placeholder_text = fieldset.find('label', class_='input-group').find('textarea', class_='form-control').get('placeholder') or fieldset.find('label', class_='input-group').find('input', class_='form-control').get('placeholder')
            print(header)
            print(placeholder_text)
            write_file(header, filename, foldername)
            write_file('\t'+placeholder_text, filename, foldername)
        except Exception as e3:
            pass


        try:
            options = fieldset.find('label', class_='select').find('select', class_='form-control').find_all('option')#[1:]
            print(header)
            write_file(header, filename, foldername)
            for option in options:
                answers = option.text
                print(answers)
                write_file('\t'+answers, filename, foldername)
        except Exception as e2:
            # print('e2',e2)
            pass

        try:
            labels = fieldset.find('div', class_='label-cont').find_all('label', class_='input-group')
            print(header)
            write_file(header, filename, foldername)
            for label in labels:
                answers = label.find('span', class_='input-group-title').text
                print(answers)
                write_file('\t'+answers, filename, foldername)
        except Exception as e1:
            # print('e1',e1)
            pass

        try:
            divs = fieldset.find('div', class_='matrix-values').find_all('div', class_='input-group input-group-matrix row')
            print(header+'(1 совсем неважное, 5 самое важное)')
            write_file(header+'(1 совсем неважное, 5 самое важное)', filename, foldername)
            for div in divs:
                answer = div.find('div', class_='col-sm-4').find('div', class_='title').text
                write_file('\t'+answer, filename, foldername)
        except Exception as e6:
            # print('e6',e6)
            pass





def parse_many_pages():
    url = 'https://www.survio.com/ru/shablony-oprosov'
    # url = 'https://www.survio.com/en/survey-templates'
    soup = BeautifulSoup(get_html(url), 'lxml')
    research = soup.find('div', class_='category research').find('ul').find_all('li')
    feedback = soup.find('div', class_='category feedback').find('ul').find_all('li')
    events = soup.find('div', class_='category events').find('ul').find_all('li')
    community = soup.find('div', class_='category community').find('ul').find_all('li')
    other = soup.find('div', class_='category other').find('ul').find_all('li')
    services = soup.find('div', class_='category services').find('ul').find_all('li')
    hr = soup.find('div', class_='category hr').find('ul').find_all('li')
    education = soup.find('div', class_='category education').find('ul').find_all('li')
    health = soup.find('div', class_='category health').find('ul').find_all('li')
    links = [research, feedback, events,community, other, services, hr, education, health]
    for li in health:
        try:
            filename = li.a.text.replace('/', ',')
            foldername = ([i for i in list(li.a.parent.parent.parent)[2]][0])
            url = li.a.get('href')
            print(filename, foldername, url)
            parse_one_page(url, filename, foldername)
        except:
            pass


def execute_parsing():
    # parse_one_page('https://www.survio.com/shablon-oprosa/otsenka-udovletvorennosti-produktom')
    parse_many_pages()


def main():
    execute_parsing()

if __name__ == '__main__':
    main()
