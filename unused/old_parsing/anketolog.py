import requests
from bs4 import BeautifulSoup

def get_html(url):
    r = requests.get(url)
    return r.text if r.ok else print(r.status_code)

def get_one_page(url):
    soup = BeautifulSoup(get_html(url), 'lxml')
    next = soup.find('div', class_='container').prettify()
    print(next)


def get_many_pages(url):
    soup = BeautifulSoup(get_html(url), 'lxml')
    divs = soup.find_all('div', class_='category-item')

    marketing = divs[0].find('ul').find_all('li')
    managment = divs[1].find('ul').find_all('li')
    client = divs[2].find('ul').find_all('li')
    social = divs[3].find('ul').find_all('li')
    b2b = divs[4].find('ul').find_all('li')
    nko = divs[5].find('ul').find_all('li')
    medic = divs[6].find('ul').find_all('li')

    for li in marketing:
    # for li in managment:
    # for li in client:
    # for li in social:
    # for li in b2b:
    # for li in nko:
    # for li in medic:
        title = li.a.text
        url = li.a.get('href')
        get_one_page(url)




def write_file(data, filename, foldername):
    with open('ancetolog/{foldername}/{filename}.txt'.format(),'a') as f:
        print(data, file=f)


def execute_parsing():
    # url = 'https://anketolog.ru/primery-anket'
    # url = 'https://anketolog.ru/primery-anket/ocenka-meropriyatiya'
    url = 'https://anketolog.ru/survey/template/preview/238'
    get_one_page(url)
    # get_many_pages()

def main():
    execute_parsing()

if __name__ == '__main__':
    main()
