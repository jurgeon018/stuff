# network - xhr
import requests
from bs4 import BeautifulSoup
import csv

def get_html(url):
    user_agent = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0'}
    r = requests.get(url, headers=user_agent)
    return r.text

def write_csv(data):
    with open('10_test.csv', 'a') as f:
        order = ['author', 'since']
        writer = csv.DictWriter(f, fieldnames=order)
        writer.writerow(data)

def get_articles(html):
    soup = BeautifulSoup(html, 'lxml')
    ts = soup.find('div', class_='testimonial-container').find_all('article')
    return ts

def get_page_data(ts):
    for t in ts:
        since = t.find('p', class_='traxer-since').text.strip()
        author = t.find('p', class_='testimonial-author').text.strip()
        data = {'author':author, 'since':since}
        write_csv(data)

def main():
    page = 1
    while True:
        url = f'https://catertrax.com/why-catertrax/traxers/page/{page}/'
        articles = get_articles(get_html(url))
        if articles:
            get_page_data(articles)
            page+=1
        else:
            break

if __name__ == '__main__':
    main()
