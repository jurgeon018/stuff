# sudo apt-get install postgresql && pgadmin3
# sudo -u postgres psql
# CREATE DATABASE test;
# \l
# sudo pip3 install peewee psycopg2 psycopg2-binary
import csv, requests
from bs4 import BeautifulSoup
from peewee import *
import re

def get_html(url):
    r = requests.get(url)
    return r.text if r.ok else print(r.status_code)


def get_data(html):
    soup = BeautifulSoup(html, 'lxml')
    trs = soup.find('table', id='currencies').find('tbody').find_all('tr')
    for tr in trs:
        tds = tr.find_all('td')
        try: name = tds[1].text.split()[1].strip()
        except: name = ''
        try: price = tds[3].a.get('data-usd')
        except: price = ''
        try: token = tds[1].text.split()[0].strip()
        except: token = ''
        try: link = 'https://coinmarketcap.com'+tds[1].find('a', class_='currency-name-container link-secondary').get('href')
        except: link = ''
        data = {'name':name, 'price':price, 'token':token, 'link':link}
        write_csv(data)


def write_csv(data):
    with open('coinmarketcap.csv', 'a') as f:
        #   csv.DictWriter
        order = ['name','price','token','link']
        writer = csv.DictWriter(f, fieldnames=order)
        writer.writerow(data)
        #   csv.writer
        # writer = csv.writer(f)
        # writer.writerow([data['name'],
        #                  data['symbol'],
        #                  data['url'],
        #                  data['price']])


def write_db():
    db = SqliteDatabase('coinmarketcap.db')
    # db = PostgresqlDatabase(database='test1', user='postgres', password='69018', host='localhost')
    # db = MySQLDatabase('my_app', user='app', password='db_password',  host='10.1.0.8', port=3316)

    class Coins(Model):
        class Meta:
            database = db
        name = CharField()
        price = FloatField()
        token = CharField()
        link = TextField()

    db.connect()
    db.create_tables([Coins])

    with open('coinmarketcap.csv', 'r') as f:
        order = ['name','price','token','link']
        reader = csv.DictReader(f, fieldnames=order)
        coins = list(reader)

        with db.atomic():
            for coin in coins:
                Coins.create(**coin)

        # with db.atomic():
        #     for i in range(0, len(plugins), 100):
        #         Plugins.insert_many(plugins[i:i+100]).execute()

        # for row in coins:
        #     coin = Coin(name=row['name'], url=row['url'], price=row['price'])
        #     coin.save()


def execute_parsing():
    url = 'https://coinmarketcap.com/'
    while True:
        soup = BeautifulSoup(get_html(url), 'lxml')
        get_data(get_html(url))
        try:
            url = 'https://coinmarketcap.com/' + soup.\
            find('ul', class_='pagination bottom-paginator').\
            find('a', text=re.compile('Next')).\
            get('href')
            print(url)
        except Exception as e:
            print(e)
            break


def main():
    execute_parsing()
    write_db()


if __name__ == '__main__':
    main()
