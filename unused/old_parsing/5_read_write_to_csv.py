import csv


def write_csv(data):
    with open('5_names.csv', 'a') as file:
        # csv.writer()
        writer = csv.writer(file)
        writer.writerow((data['name'], data['surname'], data['age']))

        # csv.DictWriter()
        order = ['name', 'surname', 'age']
        writer = csv.DictWriter(file, fieldnames=order)
        writer.writerow(data)


def read_csv():
    with open('cmc.csv') as file:
        fieldnames = ['name', 'url', 'price']
        reader = csv.DictReader(file, fieldnames=fieldnames)
        for row in reader:
            print(row)


def main():
    d = {'name': 'Petr', 'surname': 'Ivanov', 'age': 21}
    d1 = {'name': 'Ivan', 'surname': 'Ivanov', 'age': 18}
    d2 = {'name': 'Ksu', 'surname': 'Petrova', 'age': 32}

    read_csv()

    for i in [d, d1, d2]:
        write_csv(i)



if __name__ == '__main__':
    main()
