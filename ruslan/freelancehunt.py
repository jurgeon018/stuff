# URL = 'https://freelancehunt.com/projects?skills%5B%5D=28&skills%5B%5D=1&skills%5B%5D=22&skills%5B%5D=43&skills%5B%5D=124&skills%5B%5D=68&skills%5B%5D=96&skills%5B%5D=14&skills%5B%5D=131&tags%5B%5D=seo&tags%5B%5D=html&tags%5B%5D=Web-%D0%B4%D0%B8%D0%B7%D0%B0%D0%B9%D0%BD&tags%5B%5D=SMM&tags%5B%5D=%D0%B2%D0%B5%D1%80%D1%81%D1%82%D0%BA%D0%B0&tags%5B%5D=web+%D0%B4%D0%B8%D0%B7%D0%B0%D0%B9%D0%BD&tags%5B%5D=css&tags%5B%5D=%D0%BB%D0%BE%D0%B3%D0%BE%D1%82%D0%B8%D0%BF&tags%5B%5D=%D0%94%D0%B8%D0%B7%D0%B0%D0%B9%D0%BD+%D0%BB%D0%BE%D0%B3%D0%BE%D1%82%D0%B8%D0%BF%D0%B0&tags%5B%5D=%D0%B2%D0%B5%D0%B1+%D0%B4%D0%B8%D0%B7%D0%B0%D0%B9%D0%BD&tags%5B%5D=html+%D0%B2%D0%B5%D1%80%D1%81%D1%82%D0%BA%D0%B0&tags%5B%5D=%D0%B4%D0%B8%D0%B7%D0%B0%D0%B9%D0%BD+%D1%81%D0%B0%D0%B9%D1%82%D0%B0&tags%5B%5D=web+%D0%B4%D0%B8%D0%B7%D0%B0%D0%B9%D0%BD+%D1%81%D0%B0%D0%B9%D1%82%D0%BE%D0%B2&tags%5B%5D=%D0%92%D0%B5%D1%80%D1%81%D1%82%D0%BA%D0%B0+%D1%81%D0%B0%D0%B9%D1%82%D0%B0&tags%5B%5D=%D0%A0%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B0+%D1%81%D0%B0%D0%B9%D1%82%D0%B0'
URL = 'https://freelancehunt.com/projects?skills%5B%5D=28&skills%5B%5D=99'

import requests, re, csv, time
from bs4 import BeautifulSoup

def write_csv(data):
    with open('freelancehunt.csv', 'r') as f:
        file = f.read()
        if data['link'] not in file:
            with open('freelancehunt.csv', 'a') as f:
                order = ['title', 'skills', 'price', 'day', 'link']
                writer = csv.DictWriter(f, fieldnames=order)
                writer.writerow(data)

def get_html(url):
    r = requests.get(url)
    return r.text if r.ok else print(r.status_code)

def parse_one(html):
    soup = BeautifulSoup(html, 'lxml')
    trs = soup.find('table').find('tbody').find_all('tr')
    for tr in trs:
        tds = tr.find_all('td')
        try:title = tds[0].a.text
        except:title = ''
        print(title)
        try:link = 'https://freelancehunt.com'+tds[0].a.get('href')
        except: link = ''
        try:skills = tds[0].div.small.text
        except: skills = ''
        try:price = tds[1].span.text.strip()
        except:price = ''
        try:day = tds[-1].div.h2.text+tds[-1].div.h5.text
        except: day = ''

        data = {'title':title, 'link':link, 'skills':skills, 'price':price, 'day':day}
        write_csv(data)

def parse_many():
    num = 20
    while True:
        url = URL+f'&page={num}'
        soup = BeautifulSoup(get_html(url), 'lxml')
        try: next_ = soup.find('li', class_='disabled').find('a', rel='next')
        except AttributeError: next_= False
        if next_:
            parse_one(get_html(url))
            break
        else:
            parse_one(get_html(url))
            print(num)
            num += 1

def main():
    while True:
        parse_many()
        time.sleep(3600)


if __name__ == '__main__':
    main()
